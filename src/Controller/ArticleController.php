<?php

// src/Controller/ArticleController.php
namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;

class ArticleController extends AbstractController
{
    /**
     * Ajouter un article
     *
     * @Route("/admin/articles/new", name="newArticle")
     */
    public function newArticle(EntityManagerInterface $em)
    {
      $article = new Article();
      $article->setTitle('Why Asteroids Tastes Like Bacon')
        ->setSlug('why-asteroids-tastes-like-bacon-'.rand(100, 999))
        ->setContent('<<<EOF
        Spicy **jalapeno bacon** ipsum dolor amet veniam shank in dolore. Ham hock nisi landjaeger cow,
        lorem proident [beef ribs](https://baconipsum.com/) aute enim veniam ut cillum pork chuck picanha. Dolore reprehenderit
        labore minim pork belly spare ribs cupim short loin in. Elit exercitation eiusmod dolore cow
        **turkey** shank eu pork belly meatball non cupim.
        Laboris beef ribs fatback fugiat eiusmod jowl kielbasa alcatra dolore velit ea ball tip. Pariatur
        laboris sunt venison, et laborum dolore minim non meatball. Shankle eu flank aliqua shoulder,
        capicola biltong frankfurter boudin cupim officia. Exercitation fugiat consectetur ham. Adipisicing
        picanha shank et filet mignon pork belly ut ullamco. Irure velit turducken ground round doner incididunt
        occaecat lorem meatball prosciutto quis strip steak.
        Meatball adipisicing ribeye bacon strip steak eu. Consectetur ham hock pork hamburger enim strip steak
        mollit quis officia meatloaf tri-tip swine. Cow ut reprehenderit, buffalo incididunt in filet mignon
        strip steak pork belly aliquip capicola officia. Labore deserunt esse chicken lorem shoulder tail consectetur
        cow est ribeye adipisicing. Pig hamburger pork belly enim. Do porchetta minim capicola irure pancetta chuck
        fugiat.
        EOF');
      
      // publish most articles
      if (rand(1, 10) > 2) {
          $article->setPublishedAt(new \DateTime(sprintf('-%d days', rand(1, 100))));
      }

      $em->persist($article);
      $em->flush();

      return new Response(sprintf(
        'Hiya! New Article id: #%d slug: %s',
        $article->getId(),
        $article->getSlug()
      ));
    }

    /**
     * Liste des articles
     *
     * @Route("/articles", name="articles")
     */
    public function showArticles()
    {
      return $this->render('article.html.twig');
    }

    /**
     * Articles
     *
     * @Route("/articles/{slug}", name="article")
     */
    public function showArticle($slug)
    {
      return $this->render('article.html.twig');
    }
}